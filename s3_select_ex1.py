import boto3
import json

client = boto3.client('s3')

def debug_print(json_data):
    print(json.dumps(json_data, indent=4))


response = client.select_object_content(
    Bucket='jjongs-mybucket390',
    Key='test01.json',
    # Expression="SELECT s.* FROM S3Object s WHERE s.lastName = 'B'",
    Expression="SELECT s.* FROM S3Object s",
    ExpressionType='SQL',
    InputSerialization={
        'JSON': {
            'Type': 'DOCUMENT'
        }

    },
    OutputSerialization={
        'JSON': {
            'RecordDelimiter': '\n'
        }
    }
)


for event in response['Payload']:
    if 'Records' in event:
        records = event['Records']['Payload'].decode('utf-8')
        print(records)
    elif 'Stats' in event:
        statsDetails = event['Stats']['Details']
        print("Stats details bytesScanned: ")
        print(statsDetails['BytesScanned'])
        print("Stats details bytesProcessed: ")
        print(statsDetails['BytesProcessed'])